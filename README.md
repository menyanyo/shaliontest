# Shalion Test

Django Test

## Starting 🚀

1. Clone the repository in your local folder.
2. Make sure you have all the requirements installed (I recommend using a virtual environment created with Pycharm).
3. Run `manage.py makemigrations` and `manage.py migrate`
4. Execute `manage.py runserver` and you will have the django app at the default port (localhost:8000)

## Testing ⚙️

Run the tests `manage.py test`

### Endpoints available 🔩

1. See the project documentation on: `localhost:8000/documentation`
2. Execute and use all the endpoints through the simple built-in frontend on `localhost:8000/`

## Built with 🛠️

* [Django](https://docs.djangoproject.com/en/3.0/) - Framework used as MVC.
* [Django Rest Framework](https://www.django-rest-framework.org/) - Framework for building Web APIs.
* [Django Filter](https://django-filter.readthedocs.io/en/stable/) - Extension for building filters in a easier way.
* [Drf-yasg ](https://drf-yasg.readthedocs.io/en/stable/) - Automatic documentation creator.
