from rest_framework import viewsets, mixins
from django_filters import rest_framework as filters

from schools.serializers import SchoolSerializer
from schools.models import School


class SchoolFilter(filters.FilterSet):
    """ Define the filtering options for the schools """

    name = filters.CharFilter(
        lookup_expr='icontains',
    )

    class Meta:
        model = School
        fields = ('name', )


class SchoolViewSet(mixins.CreateModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.DestroyModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    """
    API endpoint that allows schools to be viewed or edited.
    """

    queryset = School.objects.all()
    serializer_class = SchoolSerializer
    filterset_class = SchoolFilter
