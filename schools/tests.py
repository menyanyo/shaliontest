from django.test import TestCase

from students.models import Student
from schools.models import School


class StudentTest(TestCase):
    def setUp(self):
        school = School.objects.create(name='Django School', capacity=2)
        Student.objects.create(first_name='Marc', last_name='Bernardo', school=school)

    def test_max_capacity_reached(self):
        """ Testing max capacity reached method for the schools """

        school = School.objects.get(name='Django School')
        self.assertEqual(school.max_capacity_reached(), False)

        # Adding the last Student to the school
        Student.objects.create(first_name='Hello', last_name='World', school=school)
        self.assertEqual(school.max_capacity_reached(), True)
