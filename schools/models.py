from django.db import models


class School(models.Model):
    """
    Represents each school, with its name and its maximum capacity for students (capacity)
    """

    # ---------- ATTRIBUTES ----------
    name = models.CharField(
        max_length=20,
        unique=True,
    )
    capacity = models.IntegerField()

    # ---------- METHODS ----------
    def __str__(self):
        return self.name

    def max_capacity_reached(self):
        """
        Returns True if the max capacity has been reached and no student can join the school
        """

        return self.students.count() >= self.capacity
