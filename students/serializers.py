from rest_framework import serializers
from students.models import Student
from schools.models import School


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = [
            'id',
            'first_name',
            'last_name',
            'school',
        ]

    def validate_school(self, name):
        """ Check if the school is already full """

        if School.objects.get(name=name).max_capacity_reached():
            raise serializers.ValidationError("School max capacity has been reached")
        return name


