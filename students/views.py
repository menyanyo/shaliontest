from django.db.models import Q
from rest_framework import viewsets, mixins
from django_filters import rest_framework as filters

from students.serializers import StudentSerializer
from students.models import Student


class StudentFilter(filters.FilterSet):
    """ Define the filtering options for the schools """

    name = filters.CharFilter(
        method='filter_by_name',
        label="First/Last name"
    )

    class Meta:
        model = Student
        fields = ('name', )

    def filter_by_name(self, queryset, name, value):
        return Student.objects.filter(
            Q(first_name__icontains=value) | Q(last_name__icontains=value),
        )


class StudentViewSet(mixins.CreateModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.DestroyModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    """
    API endpoint that allows students to be viewed or edited.
    """

    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    filterset_class = StudentFilter
