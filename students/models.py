from django.db import models
from schools.models import School


class Student(models.Model):
    """
    Represents each student, with his/her first name, last_name and the school he/she belongs to.
    """

    # ---------- ATTRIBUTES ----------
    first_name = models.CharField(
        max_length=20,
    )
    last_name = models.CharField(
        max_length=20,
    )
    school = models.ForeignKey(
        School,
        related_name='students',
        on_delete=models.CASCADE,
    )
