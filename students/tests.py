from django.test import TestCase
from django.db.utils import IntegrityError
from rest_framework import serializers

from students.models import Student
from students.serializers import StudentSerializer
from schools.models import School


class StudentTest(TestCase):
    def setUp(self):
        school = School.objects.create(name="Django School", capacity=2)
        Student.objects.create(first_name="Marc", last_name="Bernardo", school=school)

    def test_add_student_to_available_school(self):
        """ A student can be added to a school that hasn't been reached its max capacity """

        serializer = StudentSerializer()
        self.assertEqual(
            serializer.validate_school('Django School'),
            'Django School'
        )

    def test_add_student_to_unavailable_school(self):
        """ A student can be added to a school that has been reached its max capacity """

        Student.objects.create(first_name="Foo", last_name="Bar", school=School.objects.get(name='Django School'))
        serializer = StudentSerializer()
        self.assertRaises(
            serializers.ValidationError,
            serializer.validate_school,
            name='Django School'
        )

    def test_school_unique_name(self):
        """ It's not posible to have 2 schools with the same name """

        self.assertRaises(
            IntegrityError,
            School.objects.create,
            name='Django School',
            capacity=5,
        )
